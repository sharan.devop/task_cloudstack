* Take a AWS EC2 Ubuntu 18 Server for Ansible Master
* Install Ansible, run as root
 $ apt update
 $ apt install -y ansible
* Copy pem file to /etc/ansible on Ansible Master machine
* Clone the Ansible Demo project Repo
* cd demorepo/ansible
* Update the demohost file appropriately with IP & USER
* Run the below as ubuntu user
 $ sudo ansible-playbook -i demohosts dockersetup.yml

Note: Logout & Login to run docker as ubuntu
====================================================

